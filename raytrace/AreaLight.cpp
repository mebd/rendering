// 02562 Rendering Framework
// Written by Jeppe Revall Frisvad, 2011
// Copyright (c) DTU Informatics 2011

#include <optix_world.h>
#include "IndexedFaceSet.h"
#include "ObjMaterial.h"
#include "mt_random.h"
#include "cdf_bsearch.h"
#include "HitInfo.h"
#include "AreaLight.h"

using namespace optix;

bool AreaLight::sample(const float3& pos, float3& dir, float3& L) const

{
  // Get geometry info
  const IndexedFaceSet& geometry = mesh->geometry;
	const IndexedFaceSet& normals = mesh->normals;
	const float no_of_faces = static_cast<float>(geometry.no_faces());

  // Compute output and return value given the following information.
  //
  // Input:  pos  (the position of the geometry in the scene)
  //
  // Output: dir  (the direction toward the light)
  //         L    (the radiance received from the direction dir)
  //
  // Return: true if not in shadow
  //
  // Relevant data fields that are available (see Light.h and above):
  // shadows             (on/off flag for shadows)
  // tracer              (pointer to ray tracer)
  // geometry            (indexed face set of triangle vertices)
  // normals             (indexed face set of vertex normals)
  // no_of_faces         (number of faces in triangle mesh for light source)
  // mesh->surface_area  (total surface area of the light source)
  //
  // Hints: (a) As an approximation, you can use the mean of the vertex 
  //        positions and normals as the position and normal of the light.
  //        (b) Use the function get_emission(...) to get the radiance
  //        emitted by a triangle in the mesh.
  
	uint3 face;
	float3 v0, v1, v2, Le, mv, n, vn0, vn1, vn2, omegai;
	L = make_float3(0.0);

	Aabb bbox = mesh->compute_bbox();


	dir = bbox.center() - pos;
	float r = length(dir);
	dir = normalize(dir);


	Ray rt;
	rt.origin = pos;
	rt.direction = -dir;

	rt.tmin = 1e-5;
	rt.tmax = RT_DEFAULT_MAX;
	HitInfo hi;

	for (int i = 0; i < no_of_faces; i++) {
		mesh->intersect(rt, hi, i);
		if (hi.has_hit) {
			L = make_float3(0.0);
			return false;
		}
	}



	for (int i = 0; i < no_of_faces; i++) {
		face = geometry.face(i);
		Le = get_emission(i);

		vn0 = normals.vertex(face.x);
		vn1 = normals.vertex(face.y);
		vn2 = normals.vertex(face.z);

		n = normalize(vn0 + vn1 + vn2);

		L += Le * (dot(-dir, n) * mesh->face_areas[i] );
	}

	L /= r * r;

  return true;  
}

bool AreaLight::emit(Ray& r, HitInfo& hit, float3& Phi) const
{
  // Generate and trace a ray carrying radiance emitted from this area light.
  //
  // Output: r    (the new ray)
  //         hit  (info about the ray-surface intersection)
  //         Phi  (the flux carried by the emitted ray)
  //
  // Return: true if the ray hits anything when traced
  //
  // Relevant data fields that are available (see Light.h and Ray.h):
  // tracer              (pointer to ray tracer)
  // geometry            (indexed face set of triangle vertices)
  // normals             (indexed face set of vertex normals)
  // no_of_faces         (number of faces in triangle mesh for light source)
  // mesh->surface_area  (total surface area of the light source)
  // r.origin            (starting position of ray)
  // r.direction         (direction of ray)

  // Get geometry info
  const IndexedFaceSet& geometry = mesh->geometry;
	const IndexedFaceSet& normals = mesh->normals;
	const float no_of_faces = static_cast<float>(geometry.no_faces());

  // Sample ray origin and direction
 
  // Trace ray
  
  // If a surface was hit, compute Phi and return true

  return false;
}

float3 AreaLight::get_emission(unsigned int triangle_id) const
{
  const ObjMaterial& mat = mesh->materials[mesh->mat_idx[triangle_id]];
  return make_float3(mat.ambient[0], mat.ambient[1], mat.ambient[2]);
}
