// 02562 Rendering Framework
// Written by Jeppe Revall Frisvad, 2011
// Copyright (c) DTU Informatics 2011

#include <optix_world.h>
#include "HitInfo.h"
#include "mt_random.h"
#include "PointLight.h"

using namespace optix;

bool PointLight::sample(const float3& pos, float3& dir, float3& L) const
{
	//float dist = sqrt((pos.x - light_pos.x)*(pos.x - light_pos.x) + (pos.y - light_pos.y)*(pos.y - light_pos.y) + (pos.z - light_pos.z)*(pos.z - light_pos.z));
	float dist = length(pos - light_pos);
	L = intensity / (dist*dist);
	Ray shadow_ray;

	dir = normalize(light_pos - pos);
	float tmax = 1e-5;
	if (shadows) {
		shadow_ray.origin = pos;
		shadow_ray.direction = dir;
		shadow_ray.tmax = dist - tmax;
		shadow_ray.tmin = tmax;
		HitInfo shadow_info;
		tracer->trace_to_any(shadow_ray, shadow_info);
		if (shadow_info.has_hit) {
			return false;
		}
		else {
			return true;
		}
	}

  // Compute output and return value given the following information.
  //
  // Input:  pos (the position of the geometry in the scene)
  //
  // Output: dir (the direction toward the light)
  //         L   (the radiance received from the direction dir)
  //
  // Return: true if not in shadow
  //
  // Relevant data fields that are available (see PointLight.h and Light.h):
  // shadows    (on/off flag for shadows)
  // tracer     (pointer to ray tracer)
  // light_pos  (position of the point light)
  // intensity  (intensity of the emitted light)
  //
  // Hint: Construct a shadow ray using the Ray datatype. Trace it using the
  //       pointer to the ray tracer.

  return true;
}

bool PointLight::emit(Ray& r, HitInfo& hit, float3& Phi) const
{
  // Emit a photon by creating a ray, tracing it, and computing its flux.
  //
  // Output: r    (the photon ray)
  //         hit  (the photon ray hit info)
  //         Phi  (the photon flux)
  //
  // Return: true if the photon hits a surface
  //
  // Relevant data fields that are available (see PointLight.h and Light.h):
  // tracer     (pointer to ray tracer)
  // light_pos  (position of the point light)
  // intensity  (intensity of the emitted light)
  //
  // Hint: When sampling the ray direction, use the function
  //       mt_random() to get a random number in [0,1].

  // Sample ray direction and create ray

  // Trace ray
  
  // If a surface was hit, compute Phi and return true

	bool enoughPhotons = false;
	float x = 0.0, y = 0.0, z = 0.0;
	float tmax = 1e-5;
	r.origin = light_pos;

	float3 d = make_float3(0,0,0);

	do {
		x = mt_random() * 2 - 1;
		y = mt_random() * 2 - 1;
		z = mt_random() * 2 - 1;
	} while (x*x + y*y + z*z > 1);

	d = normalize(make_float3(x, y, z));

	r.direction = d;
	r.tmax = RT_DEFAULT_MAX;
	r.tmin = 1e-5;

	tracer->trace_to_closest(r, hit);
	if (hit.has_hit) {
		Phi = intensity * 4 * M_PIf;
		return true;
	}


  return false;
}
